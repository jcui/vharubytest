require 'helpers'

describe GameFrame do
	it 'should create Frame without error' do
		expect(GameFrame.new([1, 2])).to be_a GameFrame
  end

	it 'should return correct frame score' do
		expect(GameFrame.new([1, 2]).score).to eq 3
	end

	it 'should return strike?? true' do
		expect(GameFrame.new([10]).bounce).to eq 2
		expect(GameFrame.new([10]).strike?).to be_truthy
		expect(GameFrame.new([10, 0]).strike?).to be_truthy
		expect(GameFrame.new([0, 10]).strike?).to be_truthy
		expect(GameFrame.new([1, 6]).strike?).not_to be_truthy
	end

	it 'should return spare? true' do
		expect(GameFrame.new([6, 4]).bounce).to eq 1
		expect(GameFrame.new([1, 9]).spare?).to be_truthy
		expect(GameFrame.new([6, 4]).spare?).to be_truthy
		expect(GameFrame.new([6, 1]).spare?).not_to be_truthy
	end
end

