require 'helpers'

describe BowlingCalculator do
	it 'should raise invalid max role' do
		expect{
			BowlingCalculator.new('8 9 9 8')
    }.to raise_error(InvalidFrameScore)
  end

	it 'should create calculator without error' do
    expect{
			BowlingCalculator.new('1 2 3 9')
    }.to raise_error(InvalidFrameScore)
	end

	it 'should create 2 game frames' do
		calculator = BowlingCalculator.new('1 2 3 6')
		expect(calculator.frames.length).to eq 2
	end


	it 'should create 4 game frames' do
		calculator = BowlingCalculator.new('1 1 1 1 10 1 1')
		expect(calculator.frames.length).to eq 4
	end


	it 'should create 2 game frames' do
		calculator = BowlingCalculator.new('1 2 3 6 9')
		expect(calculator.frames.length).to eq 2
	end

	it 'should create 3 game frames' do
		calculator = BowlingCalculator.new('1 2 3 6 9 1')
		expect(calculator.frames.length).to eq 3
	end
	
	it 'should return 10' do
		expect(BowlingCalculator.new('1 2 3 4').get_score).to eq 10
	end

	it 'should return 29' do
		expect(BowlingCalculator.new('9 1 9 1').get_score).to eq 29
	end

	it 'should return 18' do
		expect(BowlingCalculator.new('1 1 1 1 10 1 1').get_score).to eq 18
  end

	it 'should return 300' do
		expect(BowlingCalculator.new('10 10 10 10 10 10 10 10 10 10').frames.length).to eq 9
	end

	it 'should last frame should have 3 rolls' do
		expect(BowlingCalculator.new('10 10 10 10 10 10 10 10 10 10 10 10').frames.last.rolls.length).to eq 3
	end

	it 'should return 300' do
    game = BowlingCalculator.new('10 10 10 10 10 10 1 1')
		expect(game.get_score).to eq 155
  end

	it 'should return 300' do
		expect(BowlingCalculator.new('10 10 10 10 10 10 10 10 10 10 10 10').get_score).to eq 300
	end

	it 'should return 300' do
		expect(BowlingCalculator.new('10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10 10').get_score).to eq 300
	end
end