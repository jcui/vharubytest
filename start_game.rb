require_relative 'lib/bowling_calculator'
require_relative 'lib/game_frame'

loop do
  print 'Enter the roll scores: '
  calculator = BowlingCalculator.new(gets)
  puts "Total score is #{calculator.get_score}"
end