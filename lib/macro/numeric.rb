# encoding: utf-8
#
class Numeric
   def negative?
      self  < 0
   end
end