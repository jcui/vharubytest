# encoding: utf-8
#
class Array
  def sum
    inject { |sum, x| sum + x }
  end
end