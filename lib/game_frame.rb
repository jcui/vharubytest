# encoding: utf-8
#
require './lib/macro/numeric'
require './lib/macro/array'

class GameFrame
	attr_accessor :rolls

	MAX_SCORE = 10
	STRIKE_BOUNCE = 2
	SPARE_BOUNCE = 1

	def initialize(rolls = [], last_frame = false)
		raise 'Frame rolls must be array' unless rolls.is_a?(Array)
		@_last_frame = last_frame
		raise 'Frame must have at least 1 roll' if rolls.empty?
		raise 'Frame should only have 2 rolls' if !@_last_frame and rolls.length > 2
		raise 'Last frame can have max 3 rolls' if @_last_frame and rolls.length > 3
		rolls.each do |roll|
			raise 'score cannot be less than 0' if roll < 0
		end
		raise 'Frame score cannot be greater than 10' if !@_last_frame and rolls.sum > 10
		@rolls = rolls
	end

	def last_frame?
		@_last_frame
	end

	def strike?
		@_strike ||= begin
			 (@rolls.first == MAX_SCORE or @rolls.last == MAX_SCORE) and score == MAX_SCORE
		end
	end

	def spare?
		@_spare ||= begin
			@rolls.first < MAX_SCORE and @rolls.last < MAX_SCORE and score == MAX_SCORE
		end
	end

	def score
		@_score ||= @rolls.sum
	end

	def bounce
		return STRIKE_BOUNCE if strike?
		return SPARE_BOUNCE if spare?
		0
	end
end