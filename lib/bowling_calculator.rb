# encoding: utf-8
#
require_relative 'errors'

class BowlingCalculator
	attr_accessor :bounce, :frames, :score

	MAX_FRAME = 10

	def initialize(scores)
		@frames = []
		@score = 0
		@score_array = scores.split(' ').map(&:to_i)
		build_frames
	end

	def get_score
		@frames.each_with_index do |frame, index|
			@score += frame.score
			1.upto(frame.bounce) do |offset|
				if next_frame = @frames[index + offset]
					if next_frame.strike?
						@score += next_frame.score
					else
						if offset == 1
							if next_frame.last_frame?
								@score += next_frame.rolls[0] + next_frame.rolls[1]
							else
								@score += frame.spare? ? next_frame.send(:rolls).first : next_frame.score
							end
						elsif offset == 2
							@score += next_frame.send(:rolls).first
						end
					end
				end
			end
		end
		@score
	end

	private

	def build_frames
		scores = []
		@score_array.each_with_index do |score, index|
			raise InvalidFrameScore, "Max roll score is #{Frame::MAX_SCORE}" if score > GameFrame::MAX_SCORE
			is_last_frame = false
			if @frames.length == (MAX_FRAME - 1)
				is_last_frame = true
				scores.concat(@score_array[index..(index + 2)])
			elsif score == GameFrame::MAX_SCORE
				@frames << GameFrame.new([score])
				next
			elsif scores.concat([score]).sum > GameFrame::MAX_SCORE
				raise InvalidFrameScore, 'Single frame cannot have more than 10 score'
			end

			if scores.length >= 2
				@frames << GameFrame.new(scores, is_last_frame)
				scores = []
				break if is_last_frame
			end
		end
	end
end